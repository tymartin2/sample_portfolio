import React, { useState, useEffect, useContext } from 'react'; //before you will be able to consume data from the provider, you need to define first the useContext() method to be able to use the Context data. 
//we will need components from bootstrap in order to build a login page
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'; 

import { Redirect } from 'react-router-dom'; //to allow redirection in our routes lets acquire the redirect for 'react-router-dom'

import UserContext from '../UserContext'//lets acquire the UserContext data from the UserContext module.
import users from '../data/users'//lets acquire the users created from our data folder. 

//lets declare a function called Login 
  export default function Login(){
  	//use the UserContext and we are going to destructure it to access the user and set User defined in the app component
    const { user, setUser } = useContext(UserContext) 

    //states for redirecting 
    const [willRedirect, setWillRedirect] = useState(false); 

	//lets create a state hook to store authenticated user info
	const [ email, setEmail ] = useState('')
	const [ password, setPassword ] = useState('')
	
	//lets create a state hook for the message you are going to display for your users to give a response to the user's actions. 
	let [display, changeDisplay] = useState('') // you are free to display any message that you want for your users, since this is a stretch goal. 

	//lets create an effect hook in order to give a much descriptive response to the user whenever the user input data inside teh form control 
	//to do this task we are going to declare an effect hook to create "side effects" inside our page. 
	useEffect(() => { 
		console.log(`User with email: ${user.email} is an admin: ${user.isAdmin}`);
	}, [user.isAdmin, user.email]) //this parameter is optional. 

	function authenticate(e) {
		//avoid/prevent redirection via form submission. 
		e.preventDefault()

		//how will the login page know if the user is registered in our app?

		//lets create an authentication based on the imported/acquired users data. 
		const match = users.find(user => { //1. get all the users from the data file
			return (user.email === email && user.password === password);
		}) //2. see if the information entered in the form controls matches the data from the users.js file 

		//to give a proper response to the our users if the condition above is met we have to create a control structure. 
		if(match) {
			console.log(match);
			//set the details of the authenticated user in the local storage. 
			localStorage.setItem('email', email); 
			
			//this will be converted to a string once it is set/stored in the local storage. 
			localStorage.setItem('isAdmin', match.isAdmin)

			//now we need to set the global user state to have properties obtained from the local storage, so to do this target the state setter for the useState hook
			setUser({
				email: localStorage.getItem('email'),
				//lets take from match instead of the local storage to retain the boolean data type. 
				isAdmin: match.isAdmin //we relied on the isAdmin property of the match result/data to properly determin if a user is an admin or not. 
			}); 

			//once that the user details has been properly stored in the local storage, lets redirect our user to another page.  
			setWillRedirect(true); 

		} else {
			alert('Authentication Failed, No Match from records found!'); 
		}
		//clear the input fields
		setEmail('')
		setPassword('')
       

	}
	return(
		//to properly set a location once that the credentials are authenticated and valid, lets create a ternary to change the value of our state hook for our redirect component. 
		willRedirect === true 
		? <Redirect to="/courses" />
		//otherwise show/retain inside the Login page
		: 
		<>
			<h1> {display} </h1> {/*this is the stretch goal*/} 
			<Form onSubmit={(e) => authenticate(e)}>
			   {/*userEmail*/}
			   <Form.Group controlId="userEmail">
			   		<Form.Label>Email Address</Form.Label>
			   		<Form.Control type="email" placeholder="Enter Email Here" value={email} onChange={(e) => setEmail(e.target.value)} required/>
			   </Form.Group>
			   {/*password*/}
			   <Form.Group>
			   		<Form.Label>Enter Password</Form.Label>
			   		<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
			   </Form.Group>
			   
			   	   <Button variant="warning" type="submit"> Login</Button>
			   	  
			</Form>
		</>
		)
}
