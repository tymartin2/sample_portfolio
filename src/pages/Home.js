import React from 'react'; 
//import Banner from '../components/Banner'; 
import Highlights from '../components/Highlights'; 
import Course from '../components/Course'; 
import Button from '../components/Button'; 
import { ThemeContextConsumer } from '../themeContext';//now that we analyze that the home component is a Consumer, then this means we are going to acquire the context object as a consumer rather than a provider.  

export default function Home() {
	
	return(
	  <>
        <ThemeContextConsumer>
            {context => (
                <div className={`${context.theme}-container container`}>
                    <h1 className={`${context.theme}-circle circle`}> MARTIN MIGUEL</h1>
                
                   <Button />
                </div>                   
            	)
            }                 
        </ThemeContextConsumer>		      
       </>
		)
}