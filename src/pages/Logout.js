import React, { useContext, useEffect } from 'react'; //lets acquire the useEffect() method from react
import UserContext from '../UserContext';//lets acquire the needed packages to create our new page
import { Redirect } from 'react-router-dom'; 

export default function Logout() {

	//lets consume the UserContext and destructure it to access the user and unsetUser values from the context provider
	const { unsetUser } = useContext(UserContext)

	//lets invoke the unsetUser() only after the initial render. 
	useEffect(() => {
		//invoke the unsetUser() to clear the local storage of the user info
		unsetUser(); 
	})
	return (
		// <h1> You are Logged out! </h1>
		<Redirect to="/login" />
		)
}