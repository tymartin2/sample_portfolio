import React from 'react'; 
import Course from '../components/Course';//we acquired the Course Card component

import coursesData from '../data/courses';//we acquired the courses data file. 

export default function Courses() {
    //our next task is to create/display multiple course components corresponding to the content/data from our "coursesData"
    //for us to be able to display all the courses in our data file we are going to use the map()
    //we are now going to store the data from the map function inside another variable 
    const courses = coursesData.map(course => {
        return (
            //the data that we got from the data file we are trying to insert inside the Course component. 
            <Course key={course.id} course={course}/>
        )
    }) 
    return (
        <>
            { courses }
        </> 
        )
    }
    


