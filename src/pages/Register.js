import React, { useState, useEffect } from 'react'; //to create a state hook, 1st we need to acquire the method from react
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'//lets acquire the needed packages to create this new page

export default function Register(){
	//lets declare the state getter and state setters
	let [display, changeDisplay] = useState('Register Here')
    const [email, setEmail] = useState('');
    const [password1, setPassword1 ] = useState('');
    const [password2, setPassword2 ] = useState('');
    //this state for the button will determine wether the button is enabled or not. 
    const [isActiveButton, setIsActiveButton ] = useState(false);

    //this effect will run the function within, whenever email, password1, or password2 is changed. 
    useEffect(() =>{
    	//lets create now a logic to validate the data in each form control first before enabling the submit button. 
    	if((email !== '' && password1 !== '' && password2 !== '') && (password2 === password1)){
    		//this set of code will run if the condition is met.
    		setIsActiveButton(true)
    		changeDisplay("All Requirements are met!")
    	}else{
    		//this set of code will run if the condition is "not" met.
    		setIsActiveButton(false)
    	}
    })

    //lets create a function to simulate a user registration
    function registerUser(e){
    	e.preventDefault(); 
    	//lets clear all the input fields 
    	setEmail('')
    	setPassword1('')
    	setPassword2('')
    	//lets display a message in the console for the users.
    	console.log("Thank you for Registering!")
    	changeDisplay("User Successfully Created")
    	
    }
	return(
	<>
		<h1>{display}</h1>
		<Form onSubmit={(e) => registerUser(e)}>
	{/*Email Address*/}
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				<Form.Text className="text-muted">We'll never share your email with anyone else</Form.Text>
			</Form.Group>

	{/* Password */}
			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control type="password" placeholder="Input Desired Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
			</Form.Group>

    {/* Confirm Password */}
    		<Form.Group controlId="password2">
    			<Form.Label>Confirm Password</Form.Label>
    			<Form.Control type="password" placeholder="Confirm Your Password Here" value={password2} onChange={e => setPassword2(e.target.value)} required/>
    		</Form.Group>
    		  {isActiveButton ? 
                <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
            }

		</Form>
		</>
		)
}