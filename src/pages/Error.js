import React from 'react'//lets acquire the needed packages first.
import Banner from '../components/Banner'

//lets create a function for our error page
export default function Error() {
	const errorData = {
		title: "404 - Page Not Found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home Page"
	}
	return (
		<Banner errorData={errorData}/>
		)
}