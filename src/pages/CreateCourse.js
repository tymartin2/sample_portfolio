import React from 'react' //we are going to acquire the react package
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

//lets create now a function that will allow us to return a create course page

/*this create course page will require the following information:
 1. course Id
 2. course name
 3. description
 4. price
 5. start date 
 6. end date
 7. Button (variant="select a color that you like" type="submit")
*/
export default function CreateCourse(){
	return(
	<Form>
		{/*Course ID*/}
		<Form.Group controlId="courseId">
			<Form.Label>Course ID:</Form.Label>
			<Form.Control type="text" placeholder="Enter Course ID" required/>
		</Form.Group>

	{/*Course Name*/}
	   <Form.Group controlId="courseName">
	   	   <Form.Label>Course Name:</Form.Label>
	   	   <Form.Control type="text" placeholder="Enter Course Name" required/>
	   </Form.Group>

   {/*Course Description*/}	   
	<Form.Group controlId="courseDescription"> 
 		 <Form.Label> Course Description: </Form.Label>
 		 <Form.Control as="textarea" rows="3" placeholder="Enter course description" required/>
	</Form.Group>

	{/*Course Price:*/}
     <Form.Group>
     	<Form.Label>Course Price:</Form.Label>
     	<Form.Control type="number" placeholder="Enter Course Price" required/>
     </Form.Group>
	{/*Course Start Date:*/}
     <Form.Group>
     	<Form.Label>Start Date: </Form.Label>
     	<Form.Control type="date" placeholder="Enter Start Date Here" required/>
     </Form.Group>	
	{/*Course End Date:*/}
     <Form.Group>
     	<Form.Label>End Date: </Form.Label>
     	<Form.Control type="date" placeholder="Enter End Date Here" required/>
     </Form.Group>

     <Button variant="success" type="submit">Create Course</Button>
	</Form>			
		)
}