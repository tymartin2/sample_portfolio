import React from 'react'; 
import Jumbotron from 'react-bootstrap/Jumbotron' //now we are going to acquire a Jumbotron component from our react-bootstrap package
import { Link } from 'react-router-dom';
//import Button from 'react-bootstrap/Button'
//we are going to acquire first the bootstrap grid system
import { Row } from 'react-bootstrap'; 
import { Col } from 'react-bootstrap'; 

//next thing that we are going to do is create a function to return our Banner component
export default function  Banner(data){
	//lets destructure the props of the data error into it's designated properties. 
	const {title, content, destination, label} = data
	return(
		<Row>
			<Col>
				<Jumbotron>
					<h1> {title} </h1>
					<p> {content}</p>
					<Link to={destination}>{label
					}</Link>
				</Jumbotron>
			</Col>
		</Row>
		)
} 

