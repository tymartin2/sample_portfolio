import React, { useContext } from 'react' //step 2: define the useContext function before you start consuming and destructuring the information from the Context file.
//we need to import the necessary boostrap components in order for us to utilize the pre-setted bootstrap. 
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'

//for us to be able to do the task 
//step 1. we need to acquire the userContext module for us to be able to consume the data and destructure the information it holds. 
import UserContext from '../UserContext'

export default function NavBar(){
	//step 3: lets now consume the UserContext and lets destructure it to access the user values from the context provider. 
    const { user } = useContext(UserContext)

	return(
			<Navbar bg="light" expand="lg">
				{(user.email !== null) ? 
					<>
					  <h1>M </h1>
					  <Navbar.Brand href="/"> {user.email} </Navbar.Brand>
					</>
					:
					<>
					<Navbar.Brand href="/"><img src="https://img.icons8.com/dusk/64/000000/user-male.png"/> </Navbar.Brand>
					</>
				}
				{/*you can rename this with you personal brand*/}
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="mr-auto">
					
						<Nav.Link href="/register">Developer</Nav.Link>
						<Nav.Link href="/login">Delights </Nav.Link>
			   					
					</Nav>
				</Navbar.Collapse>
			</Navbar>
		)
}
