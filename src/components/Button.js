import React from 'react'
import { ThemeContextConsumer } from '../themeContext'

export default function Button(props) { //for it to consume the data from the provider add props inside the argument scope.
	return (
		 <ThemeContextConsumer>
		    {context => (
		    	<button onClick={context.toggleTheme} className="button">
		    	   Switch Theme 
		    	</button>
		    	)
		    }
		 </ThemeContextConsumer>
		);
}