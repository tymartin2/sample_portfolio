import React, {useState, useEffect} from 'react'// import the necessary packages in order to create this new component. 

import Card from 'react-bootstrap/Card'; //Get a Card Component from react-bootstrap
import Button from 'react-bootstrap/Button';// Button component from react-bootstrap 
import PropTypes from 'prop-types'

//inside the argument scope of the function we are going to declare its props object
export default function Course({course}) {
	//we need now to destructure the course prop into its properties
	const { name, description, price } = course; 
	//now we are going to use a state hook for this component to able to store the state of the number of students enrolled for this class. 

	//when calling a useState hook we to set the initial state that will always return 2 things; state getter and state setter
	const [count, setCount] = useState(0); 
	//lets create a state hook for the number of seats available in our class. 
	const [seats, setSeat] = useState(15);
	//lets declare another state hook that will describe the availability of a course
	const [isOpen, setIsOpen ]= useState(true); 


	//we have defined the useEffect() to have this component do something after every DOM update.
	//the useEffect determines what would happen if the event has been triggered
		//this is for checking for the availability for enrollment of a course, we placed the control structure to get the initial state for the seats because it will be best suited here. 
	//this will run both after the initial render and for every sebsquent DOM update
	useEffect(() => {
		if(seats === 0){
			setIsOpen(false)
		}
	}, [seats]); //[seats] is an OPTIONAL parameter, react will re-run this effect only if any of the values contained in this array has changed from thae last render/update.
    // ---> [seats] -> inputs, the inputs tells react to watch these inputs whenever they get updated.   

	//lets create a function that will allow us to keep track of the changes in the number of enrollees, we would like to name this function as enroll

	// now that we have indicated the number of slots/seats available in our class, the seat number should be subtracted to the number of enrollees for the course
	function enroll() {
		//we are going to set a newState for our count variable 
		setCount(count + 1); //everytime that the enroll button is clicked the number of students will rise
		setSeat(seats - 1); //while the number of seats available will be reduced. 
		//we are going to create a control structure 
	}
	return (
			<Card>
				<Card.Body>
					<Card.Title>
					    <span className="subtitle">Name: </span>
					    {name}
					</Card.Title>
					<Card.Text>
						<span className="subtitle">Description:</span>
							<br />
							{description}
							<br />
						<span className="subtitle">Price:</span>
							<br />
							PhP{price}
							<br />
						<span className="subtitle">Enrollees:</span>
							<br />
							{count}
							<br />
						<span className="subtitle">Seats:</span>
						    {seats}
					</Card.Text>
					{isOpen ? 
						<Button variant="primary" onClick={enroll}>Enroll</Button>
					:
						<Button variant="primary" disabled>Enroll</Button>
					}
				</Card.Body>
			</Card>
		)
} 

//this will check if the Course component is getting the correct prop types
Course.propTypes = {
	//shape() -> is used to check that a prop object conforms to a specific shape
	course: PropTypes.shape({
		//we need to define the properties and their expected data types 
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}