import React from 'react'; //acquire the react package
//we will use the bootstrap grid systems components
import { Row } from 'react-bootstrap'; //this is an example of a named export, if it is a named export, you cannot "repackage" it into another variable or container 
import { Col } from 'react-bootstrap'; 

import Card from 'react-bootstrap/Card'//we are going to acquire the Card component from react-bootstrap, this is also an example of a export default, you can name it however you like.

export default function Highlights() {
	return (
		<Row>
		   <Col xs={12} md={4}>
				<Card className="cardHighlight">
				    <Card.Body>
				    	<Card.Title>Learn From Home</Card.Title>
				         <Card.Text>
				              Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Nisi, quidem! Repellendus ea harum consectetur 
				         </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
			  <Col xs={12} md={4}>
				<Card className="cardHighlight">
				    <Card.Body>
				    	<Card.Title>Study Now, Pay Later</Card.Title>
				         <Card.Text>
				              Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Nisi, quidem! Repellendus ea harum consectetur 
				         </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
			  <Col xs={12} md={4}>
				<Card className="cardHighlight">
				    <Card.Body>
				    	<Card.Title>Be Part of Our Community</Card.Title>
				         <Card.Text>
				              Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Nisi, quidem! Repellendus ea harum consectetur 
				         </Card.Text>
				    </Card.Body>
				</Card>
			</Col>
		</Row>
		)
}