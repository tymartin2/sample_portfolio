import React from 'react';//lets acquire the react package 

//1. create a context Object using the create Context function from react. and then place it inside a variable.
const UserContext = React.createContext();

//2. We have to get the Provider component from the context Object
export const UserProvider = UserContext.Provider; //is this a named or default export?
//The Purpose of the Provider is to "hold the data" that you want to Save. 
//anu siya ?? --> container 

export default UserContext;  //default export