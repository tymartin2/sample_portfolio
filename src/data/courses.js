export default [
  {
	id: "wdc001",
	name: "PHP - Laravel",
	description: "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Corporis commodi eius fugit eveniet voluptatum, veniam ratione aspernatur, deleniti reprehenderit eligendi asperiores cupiditate libero pariatur voluptates beatae, itaque perspiciatis. Accusantium, illo.",
	price: 45000,
	instructor: 'Bruce Lee', 
	onOffer: true 
}, 
{
	id: 'wdc002',
	name: 'Python - Django',
	description: "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Corporis commodi eius fugit eveniet voluptatum, veniam ratione aspernatur, deleniti reprehenderit eligendi asperiores cupiditate libero pariatur voluptates beatae, itaque perspiciatis. Accusantium, illo.",
	price: 50000,
	instructor: 'Jet Li',
	onOffer: true  
}, 
{
	id: 'wdc003',
	name: 'Java - Springboot',
	description: "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Corporis commodi eius fugit eveniet voluptatum, veniam ratione aspernatur, deleniti reprehenderit eligendi asperiores cupiditate libero pariatur voluptates beatae, itaque perspiciatis. Accusantium, illo.",
	price: 55000,
	instructor: 'Chow Yun Fat', 
	onOffer: true
},
{
	id: 'wdc004',
	name: "Javascript - React", 
	description: "Lorem ipsum dolor, sit amet consectetur adipisicing, elit. Corporis commodi eius fugit eveniet voluptatum, veniam ratione aspernatur, deleniti reprehenderit eligendi asperiores cupiditate libero pariatur voluptates beatae, itaque perspiciatis. Accusantium, illo.",
	price: 55000,
	instructor: 'Chow Yun Fat', 
	onOffer: true
}

]