import React, { useState } from 'react';
import NavBar from './components/NavBar'
import Home from './pages/Home';
import Courses from './pages/Courses'; //it can be repackaged
import Login from './pages/Login';//we acquired the Login module by importing it and placing it inside a variable called Login
import Register from './pages/Register';
import { Container } from 'react-bootstrap'//lets acquire a container component from boostrap
import './App.css';
import Error from './pages/Error';
import CreateCourse from './pages/CreateCourse';

//routing components
import { BrowserRouter as Router } from 'react-router-dom'//we are going to acquire the react-router-dom package. 
//is it necessary or required to rename it as "Router", this just an extra step (OPTIONAL)
import { Route, Switch } from 'react-router-dom' //these components combined ared going to tell react what components/pages should be rendered on a specific URL. 

//context provider section 
import { UserProvider} from './UserContext'; //acquiring the data from the UserContext file. 
import Logout from './pages/Logout';//we are acquiring the Logout component

function App(props) { 
 //lets create a state hook for the user state, it is defined here for global scoping 
 const [user, setUser] = useState({
  //this will be initialized as an object with properties from our local storage. 
  email: localStorage.getItem('email'), 
 
  //we converted the string into boolean 
  isAdmin: localStorage.getItem('isAdmin') === 'true'
 }) // the workflow of this state hook is used so that the information from the user.js file inside the data folder will be saved in the local storage. 

 //the next function we are going to create is to "clear" the data from the local storage upon logging out. 
  const unsetUser = () => {
     //all we have to do to clear the local storage is to use the clear(). 
     localStorage.clear()
     //we also have to change/revert the values of the properties to null. 
     setUser({
       email: null,
       isAdmin: null //we now set the user global scope in the content provider to have its user props set to null. 
     })
  }
  return (
  	/*
		this is all the components inside the entry point.
  	*/
    <div className="App"> 
  {/*this STEP 8: wrap the component tree within the userProvider context provider so that the components will have access to the passed values in here */} 
  {/*will be the provider in this component tree while all the JSS components wrap inside this will serve as the its consumers.*/ }
    <UserProvider value={{user, setUser, unsetUser}}> 
      <Router>
      <NavBar />  
      <Container>   
        <Switch> 
      {/*When we try to load a page with a URL similar to another component, you are taking the risk of rendering the wrong page... so the switch component is looking for the first match, but as soon as react finds "/", it automatically renders the Home component*/}
          {/*let's use the Route Component to render components within this container based on the defined routes that we will assign to them.*/}
          <Route exact path="/" component={Home} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/courses" component={Courses} />
          <Route path="/course/create" component={CreateCourse}/>
          <Route path="/logout" component={Logout} />
          <Route component={Error}/> //it means that the consumers components can acquire/use the data being passed by the Provider. 
        </Switch>
      </Container> 
      </Router> 
      </UserProvider> 
    </div>
  );
}

export default App;

