import React, { Component } from 'react'; //acquire the component funtions of react to be able to convert our class into a react component. 
const { Provider, Consumer } = React.createContext();//lets create a new context object for the provider as well as for a consumer.
//this demonstration will be another approach on how you can get the components of context object
//if you analyze above the way we acquired the Provider and Consumer components, you will be to see that they are "named" exports

//we are going to create a context using a class method 
class ThemeContextProvider extends Component {
	state = { //the state property declared here will be the state getter
		theme: "Day"
	}; //so this is similar in creating a state hook

	//lets create a function that will allow us to change the theme/color of our page. 
	toggleTheme = () => {
        this.setState(prevState => {
        	return {
        		theme: prevState.theme === "Day" ? "Night" : "Day"
        	};
        }); //so lets create a control structure for the return scope so that the change will only happen if the user decides to call the function
	}; 

	render() {
		return (
				<Provider value={{ theme: this.state.theme, toggleTheme: this.toggleTheme }}> 
				    {this.props.children}
				</Provider>
			);
	}
}

export { ThemeContextProvider, Consumer as ThemeContextConsumer}; //by doing this component provider cannot be repackage inside another variable.  