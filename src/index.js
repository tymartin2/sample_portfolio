import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css'
import { ThemeContextProvider } from './themeContext'; 
import App from './App'; //this is the part where we acquired the data from the app.js module 


ReactDOM.render(
  <ThemeContextProvider>
    <App />
  </ThemeContextProvider>,
  document.getElementById("root")
);



